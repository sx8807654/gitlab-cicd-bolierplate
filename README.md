## Gitlab CI/CD
### Concept
#### Job
- `job`:  a **task** that gitlab should do for us
- For example, we can define `build` as a `job`
- The runner will `git clone` your repository
- The `default` directory is `/builds/{username}/{project_name}/`
```yml
build:
  script:
    - mkdir build
    - cd build
    - touch car.txt
    - echo "chassis" > car.txt
    - echo "engine" > car.txt
    - echo "wheels" > car.txt
```
- By default, Gitlab will run jobs **in parallel**
- After `git commit`, you will see two jobs running **concurrently** in Gitlab
- When **a** specific job fails, you will received the `notification` in your `email`
```yml
build:
  script:
    - mkdir build
    - cd build
    - touch car.txt
    - echo "chassis" > car.txt
    - echo "engine" > car.txt
    - echo "wheels" > car.txt
test:
  script:
    - test -f build/car.txt
    - cd build
    - grep "chassis" car.txt
    - grep "engine" car.txt
    - grep "wheels" car.txt

```
#### Stage
- We can define the **stages**(`order`) in **sequence**
- And you will see the jobs will be executed **one after one**
```yml
stages:
  - build
  - test

build:
  stage: build
  script:
    - mkdir build
    - cd build
    - touch car.txt
    - echo "chassis" > car.txt
    - echo "engine" > car.txt
    - echo "wheels" > car.txt
test:
  stage: test
  script:
    - test -f build/car.txt
    - cd build
    - grep "chassis" car.txt
    - grep "engine" car.txt
    - grep "wheels" car.txt
```
- What if we define `two jobs` under the **same** stage?
- The two jobs will be executed `in parallel`
```yml
stages:
  - build
  - test

build:
  stage: build
  script:
    - mkdir build
    - cd build
    - touch car.txt
    - echo "chassis" > car.txt
    - echo "engine" > car.txt
    - echo "wheels" > car.txt
build2:
  stage: build
  script:
    - mkdir build2
    - cd build2
    - touch car.txt
    - echo "chassis" > car.txt
    - echo "engine" > car.txt
    - echo "wheels" > car.txt
test:
  stage: test
  script:
    - test -f build/car.txt
    - cd build
    - grep "chassis" car.txt
    - grep "engine" car.txt
    - grep "wheels" car.txt
```
- Conclusion: 
  - `jobs under same stage` **in parallel**
  - different `stages` in** sequence**
----
- You can see the test job failed, why?!
- Because the folder `build/car.txt` doesn't exist.
- Jobs are `standalone` by default, they **don't depend on** each other and don't have communication to each other
#### Artifact
- How to `save` the **artifact**(`result`) of one job in order to be **shared** between jobs?
- The `build` folder including the `files inside it` will be uploaded to the `coordinator`
- The `next job` will download the `artifacts` from the `coordinator`
> **You can download and inspect the artifact in Gitlab!**
```yml
stages:
  - build
  - test

build:
  stage: build
  script:
    - mkdir build
    - cd build
    - touch car.txt
    - echo "chassis" > car.txt
    - echo "engine" > car.txt
    - echo "wheels" > car.txt
  artifacts:
    paths:
      - build/
test:
  stage: test
  script:
    - test -f build/car.txt
    - cd build
    - grep "chassis" car.txt
    - grep "engine" car.txt
    - grep "wheels" car.txt
```
- It failed, why?
- Solution: Use `>>` instead of `>` to append files
```yml
stages:
  - build
  - test

build:
  stage: build
  script:
    - mkdir build
    - cd build
    - touch car.txt
    - echo "chassis" >> car.txt
    - echo "engine" >> car.txt
    - echo "wheels" >> car.txt
  artifacts:
    paths:
      - build/
test:
  stage: test
  script:
    - test -f build/car.txt # to test if the car.txt exist
    - cd build
    - grep "chassis" car.txt
    - grep "engine" car.txt
    - grep "wheels" car.txt
```
- Now it passed
- Trick: Always remember use `ls` or `cat filename` to **debug**.

#### Runner
- Based on `docker`, you can image that as a `linux machine`
- By default, Gitlab use `Shared` Runners

#### Image
- The runner is based on docker, by default in **ruby** env.
- What if we want to change the env?
- Use `image`, find the image that you want [here](https://hub.docker.com/)
```yml
stages:
  - build
  - test

build:
  image: node:latest
  stage: build
  script:
    - mkdir build
    - cd build
    - touch car.txt
    - echo "chassis" >> car.txt
    - echo "engine" >> car.txt
    - echo "wheels" >> car.txt
  artifacts:
    paths:
      - build/
test:
  image: node:latest
  stage: test
  script:
    - test -f build/car.txt # to test if the car.txt exist
    - cd build
    - grep "chassis" car.txt
    - grep "engine" car.txt
    - grep "wheels" car.txt
```
- How to `choose` image?
  - It can `meet your requirements`
  - Keep the docker file as **small** as possible, for example user `alpine`
----

#### Global Image
- But I have to specify the `image` for **each** job!
```yml
image: node:latest
stages:
  - build
  - test

build:
  stage: build
  script:
    - mkdir build
    - cd build
    - touch car.txt
    - echo "chassis" >> car.txt
    - echo "engine" >> car.txt
    - echo "wheels" >> car.txt
  artifacts:
    paths:
      - build/
test:
  stage: test
  script:
    - test -f build/car.txt # to test if the car.txt exist
    - cd build
    - grep "chassis" car.txt
    - grep "engine" car.txt
    - grep "wheels" car.txt
```
#### Job Status
- success: if the exit status is `0`
- failed: if the exit status is `1-255`
- `Debugger skill`:
```sh
# to check if xxx exist in index.html
grep -q "xxx" index.html
# to check thee exit status
echo $?
```
> The last **green** line showed in the gitlab runner is where the test `failed`.

#### Blocking
- Some jobs like start the api server will by default stop the scripts below it from running.
- Solution: Run in backgroud
```yml
test:
  image:node
  stage:test
  script:
    - npm install
    - npm start & # core
    - sleep 3
    - {some thing else}
```

#### Environment Variable
- You can define `env var` in Gitlab `Project`
- `Env var `is accessible by gitlab `runner`
- Store the secrets in `env var`


#### Cache
- **When**: used for **temporary** `storage` for project **dependencies**.
- cache between `runnings of pipeline`, the `bigger` the cached files, the more obvious the speed is increasing
- cache between `stages`, the `more cache shared between stages`, the more obvious the speed is increasing
```yml
image: node:latest

stages:
  - build
  - test

build:
  image: node:latest
  stage: build
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - node_modules/
  script:
    - mkdir build
    - cd build
    - touch car.txt
    - echo "chassis" >> car.txt
    - echo "engine" >> car.txt
    - echo "wheels" >> car.txt
  artifacts:
    paths:
      - build/
test:
  image: node:latest
  stage: test
  script:
    - test -f build/car.txt # to test if the car.txt exist
    - cd build
    - grep "chassis" car.txt
    - grep "engine" car.txt
    - grep "wheels" car.txt
```
- When we should use `CI_COMMIT_REF_SLUG`?
  - In the example, the key of cache is specific to one branch.
  - The `CI_COMMIT_REF_SLUG` is the key of current branch.
- Global Cache
```yml
image: node:latest
stages:
  - build
  - test
  
cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - node_modules/

build:
  image: node:latest
  stage: build

  script:
    - mkdir build
    - cd build
    - touch car.txt
    - echo "chassis" >> car.txt
    - echo "engine" >> car.txt
    - echo "wheels" >> car.txt
  artifacts:
    paths:
      - build/
test:
  image: node:latest
  stage: test
  script:
    - test -f build/car.txt # to test if the car.txt exist
    - cd build
    - grep "chassis" car.txt
    - grep "engine" car.txt
    - grep "wheels" car.txt
```
> Warning: sometimes the cache will cause the job to fail, simply use `clear runner caches` in gitlab to fix this.

#### Staging
- `Staging` is simply the deployment to a environment for `UAT`.

#### Environment
- Easily to track
- You could open the environment directly in `Gitlab > Operations > Environemnts` 
```yml
deploy staging:
  stage: deploy staging
  environment:
    name: staging
    url: https://instance.surge.sh
  script:
    ...
```
- You can also set as `dynamic` `environment`
```yml
deploy staging:
  stage: deploy staging
  environment:
    name: staging/$CI_COMMIT_REF_NAME # core
    url: https://instance-staging/$CI_COMMIT_REF_NAME.surge.sh
  script:
    ...
```

#### Variables
- To store `common` things that will be used in gitlab `runner`
- You can store `domain name`, for example
- You could define in gitlab
- Or you could define in `.gitlab-ci.yml`
```yml
variables:
  STAGING_DOMAIN: instazone-staging.surge.sh
  PRODUCTION_DOMAIN: instazone.surge.sh

# in some place for use
$STAGING_DOMAIN
```

#### Auto Trigger
- Anytime you make changes to branches of the gitlab project, it will trigger the CI `rule` that has been defined in the `.gitlab-ci.yml`
- `.gitlab-ci.yml` can also define the rule for **different** `branches` of the project.

#### Manual Trigger
- Happens between `staging` and `production`
- `Opiton 1`: **Fully Manually**
```yml
deploy production:
  stage: deploy production
  environment:
    name: production
    url: $PRODUCTION_DOMAIN
  when: manual
  script:
    - npm install --global surge
    - surge --project ./public --domain $PRODUCTION_DOMAIN
```
> The job behind the manual job will be automatically failed.
- If you don't want the job behind to fail
```yml
deploy production:
  stage: deploy production
  environment:
    name: production
    url: $PRODUCTION_DOMAIN
  when: manual
  allow_failure: false
  script:
    - npm install --global surge
    - surge --project ./public --domain $PRODUCTION_DOMAIN
```
- `Opiton 1`: **Branching and Merging**
  - For branch `develop`, we only need `build` and `test`, without `staging` and `deploy`
  - For branch `master`, we all jobs to run
```yml
deploy staging:
  stage: deploy staging
  environment:
    name: staging
    url: <url>
  only: # core
    - master
  script:
    ...

deploy production:
  stage: deploy production
  environment:
    name: production
    url: <url>
  only: # core
    - master
  script:
    ...
```
#### [On_stop](https://docs.gitlab.com/ee/ci/yaml/#environmenton_stop)
- You want to start a runner after one environment stops
```yml
review_app:
  stage: deploy
  script: make deploy-app
  environment:
    name: review
    on_stop: stop_review_app # when the environment stop, trigger stop_review_app job

stop_review_app:
  stage: deploy
  variables:
    GIT_STRATEGY: none # won't clone git repo
  script: make delete-app
  when: manual # won't be executed automatically
  environment:
    name: review
    action: stop
```

#### Disable job
- Enable
```yml
job:
  ...
```
- Disable
```yml
.job:
  ...
```
## Reference
[Gitlab CI/CD Pipeline Configuration Reference](https://docs.gitlab.com/ce/ci/yaml/#stages)

[Gitlab Predefined Environment Variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)

[Free Personal Static Website Tutorial](https://www.udemy.com/course/gitlab-ci-pipelines-ci-cd-and-devops-for-beginners/learn/lecture/14305192#overview)

[Static Website Easy Deploy](https://surge.sh/pricing)

[Job Templates and Anchor](https://www.udemy.com/course/gitlab-ci-pipelines-ci-cd-and-devops-for-beginners/learn/lecture/15571066#overview)

